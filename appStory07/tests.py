import unittest
from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class Story07_unit_test(TestCase):
    def test_story7_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_Story07_using_base_func(self):
        found = resolve('/')
        self.assertEquals(found.func, base)
    
    def test_Story07_using_story07_html(self):
        response = Client().post('/')
        self.assertTemplateUsed(response, 'base.html')

class Story07_HTML__content_unit_test(TestCase):
    def test_html_contains_greeting(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello", response_content)

    def test_Aktivitas_in_html_is_exist(self):
        request = HttpRequest()
        response = base(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Aktivitas saat ini', html_response)
    
    def test_Pengalaman_in_html_is_exist(self):
        request = HttpRequest()
        response = base(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Pengalaman Organisasi/Kepanitiaan', html_response)
    
    def test_Prestasi_in_html_is_exist(self):
        request = HttpRequest()
        response = base(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Prestasi', html_response)

    def test_button_UbahTema_in_html_is_exist(self):
        request = HttpRequest()
        response = base(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Ubah Tema', html_response)



    
