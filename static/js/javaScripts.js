$(document).ready(function() {
    $( "#accordion" ).accordion({
        heightStyle: "content",
        active:false,
        collapsible: true,
        header:"div.accordionheader"
        });
    $("#accordion").accordion();
    //set accordion header options
    $("#accordion").accordion("option", "icons", {
        'header': 'icon-expand',
        'activeHeader': 'icon-collapse'
    });


    var clicked = false;
    $("#btn").click(function() {
        if(clicked){
            $('body').css('background-color', 'rgb(139, 175, 170)');
            $('.accordionheader').css('background-color', '#ACD4CE');
            $('.accordionbody ul li').css('border-bottom', 'rgb(124, 166, 190)');
            $('.accordionbody').css('background', '#7aacaf');
            $('.ui-state-active i').css('color', '#ACD4CE');
            clicked = false;
        }else{ 
            $('body').css('background-color', 'rgb(219, 208, 186)');
            $('.accordionheader').css('background-color', 'rgb(197, 184, 157)');
            $('.accordionbody ul li').css('border-bottom', 'rgb(151, 137, 111)');
            $('.accordionbody').css('background', ' rgb(177, 165, 142)');
            $('.ui-state-active i').css('color', 'rgb(197, 184, 157)');
           
            clicked = true;
        };
        return false;
    });

});